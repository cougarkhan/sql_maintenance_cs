﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.IO;

namespace sql_maintenance
{
    public class Settings
    {
        public Sqlserver sqlserver { get; set; }

        public Settings(string path_to_config_file)
        {
            try
            {
                JavaScriptSerializer ser = new JavaScriptSerializer();
                string json = File.ReadAllText(path_to_config_file);
                List<RootObject> configs = ser.Deserialize<List<RootObject>>(json);
                sqlserver = configs[0].sqlserver;
            }
            catch (Exception e)
            {
            }
        }
    }

    public class Instance
    {
        public string name { get; set; }
    }

    public class Databases
    {
        public List<string> name { get; set; }
    }

    public class CleanupFiles
    {
        public bool enabled { get; set; }
        public string source_folder { get; set; }
        public int file_age_days { get; set; }
    }

    public class CheckIntegrity
    {
        public bool enabled { get; set; }
    }

    public class IndexDefrag
    {
        public bool enabled { get; set; }
    }

    public class UpdateStatistics
    {
        public bool enabled { get; set; }
    }

    public class BackupDatabase
    {
        public bool enabled { get; set; }
        public string destination_folder { get; set; }
    }

    public class MoveFiles
    {
        public bool enabled { get; set; }
        public string source_folder { get; set; }
        public string destination_folder { get; set; }
    }

    public class Options
    {
        public CleanupFiles cleanup_files { get; set; }
        public CheckIntegrity check_integrity { get; set; }
        public IndexDefrag index_defrag { get; set; }
        public UpdateStatistics update_statistics { get; set; }
        public BackupDatabase backup_database { get; set; }
        public MoveFiles move_files { get; set; }
    }

    public class Sqlserver
    {
        public Instance instance { get; set; }
        public Databases databases { get; set; }
        public Options options { get; set; }
    }

    public class RootObject
    {
        public Sqlserver sqlserver { get; set; }
    }
}
